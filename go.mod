module github.com/rrouzbeh/cyborg-hunt

go 1.14

require (
	github.com/ClickHouse/clickhouse-go v1.4.0
	github.com/Shopify/sarama v1.26.1
	github.com/oschwald/geoip2-golang v1.4.0
	github.com/prometheus/client_golang v1.5.1
	github.com/prometheus/common v0.9.1
)
