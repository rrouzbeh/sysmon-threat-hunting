package util

import (
	"crypto/sha1"
	"encoding/hex"
	"os"
	"strconv"
	"strings"
	"time"
)

// GetEnv Handel os.LookupEnv
func GetEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}

	return defaultVal
}

func MakeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Second)
}

func Hash(s string) string {
	h := sha1.Sum([]byte(s))
	return hex.EncodeToString(h[:])
}

func RemoveAfter(value string, a string) string {
	// Get substring before a string.
	pos := strings.Index(value, a)
	if pos == -1 {
		return ""
	}
	return value[0:pos]
}

func RemoveBefor(value string, a string) string {
	// Get substring after a string.
	pos := strings.LastIndex(value, a)
	if pos == -1 {
		return ""
	}
	adjustedPos := pos + len(a)
	if adjustedPos >= len(value) {
		return ""
	}
	return value[adjustedPos:len(value)]
}

func ParseBool(s string) bool {
	b, _ := strconv.ParseBool(s)

	return b
}
