package click

import (
	"database/sql"
	"fmt"
	"os"

	"github.com/rrouzbeh/cyborg-hunt/pkg/parser"
	"github.com/rrouzbeh/cyborg-hunt/pkg/prom"
)

// SecurityPrepare prepare data for insert
func SecurityPrepare(db *sql.DB) (*sql.Tx, *sql.Stmt) {
	Tx, _ := db.Begin()
	Stmt, err := Tx.Prepare("INSERT INTO cyborg.security (event_date_creation, host_name, computer_name, event_id, additionalinfo, additionalinfo2, certificate_hash_sha1, certificate_issuer, certificate_serial_number, class_id, class_name, compatible_ids, computer_account_change, device_description, device_id, dsobject_attribute_name, dsobject_attribute_type, dsobject_attribute_value, dsobject_class, dsobject_dn, dsobject_domain_type, dsobject_guid, dsoperation_app_correlation_id, dsoperation_correlation_id, dsoperation_type, dst_ip_addr, dst_port, event_failure_reason, event_status, event_sub_status, host_domain, host_primary_group_id, location_information, logon_authentication_lan_package_name, logon_authentication_package_name, logon_elevated_token, logon_impersonation_level, logon_key_length, logon_process_name, logon_restricted_admin_mode, logon_transmitted_services, logon_type, logon_virtual_account, master_key_id, network_protocol, object_access_list, object_access_mask, object_access_reason, object_handle_id, object_name, object_operation_type, object_privilege_list, object_properties, object_resource_attributes, object_server, object_type, object_value_old_type, policy_category_id, policy_changes, policy_subcategory_guid, policy_subcategory_id, process_id, process_name, process_path, recovery_key_id, registry_key_path, registry_key_value_data, registry_key_value_name, registry_key_value_type, registry_value_old_data, scheduled_task_content, scheduled_task_name, service_account_name, service_image_path, service_name, service_sid, service_start_type, service_type, share_access_mask, share_local_path, share_name, share_relative_target_name, source_host_name, src_host_name, src_ip_addr, src_port, target_host_account_control, target_host_account_expires, target_host_allowed_to_delegate, target_host_display_name, target_host_dns_host_name, target_host_domain, target_host_home_directory, target_host_home_oath, target_host_logon_hours, target_host_name, target_host_new_uac_value, target_host_old_uac_value, target_host_parameters, target_host_password_last_set, target_host_principal_name, target_host_profile_path, target_host_sam_name, target_host_script_path, target_host_service_principal_names, target_host_sid, target_host_sid_history, target_host_user_workstations, target_user_account_control, target_user_account_expires, target_user_allowed_to_delegate, target_user_display, target_user_display_name, target_user_domain, target_user_home_directory, target_user_home_path, target_user_logon_hours, target_user_name, target_user_new_name, target_user_new_uac_value, target_user_old_name, target_user_old_uac_value, target_user_parameters, target_user_password_last_set, target_user_primary_group_id, target_user_principal_name, target_user_profile_path, target_user_sam_name, target_user_script_path, target_user_sid, target_user_sid_history, target_user_workstations, ticket_encryption, ticket_encryption_type, ticket_options, ticket_pre_auth_type, ticket_status, token_restricted_sid_count, transaction_guid, trust_attributes, trust_direction, trust_filtering, trust_name, trust_sid, trust_type, user_access_list, user_access_reason, user_attribute_dummy, user_domain, user_linked_logon_id, user_logon_guid, user_logon_id, user_name, user_network_account_domain, user_network_account_name, user_primary_group_id, user_privilege_list, user_reporter_domain, user_reporter_id, user_reporter_name, user_reporter_sid, user_sid, vendor_ids) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)")
	if err != nil {
		fmt.Println("Prepare Error:", err)
		os.Exit(0)
	}
	return Tx, Stmt
}

// SecurityInsert Insert event to clickhouse
func SecurityInsert(Stmt *sql.Stmt, s parser.Security) (err error) {

	_, err = Stmt.Exec(s.EventDateCreation, s.HostName, s.ComputerName, s.EventID, s.AdditionalInfo, s.AdditionalInfo2, s.CertThumbprint, s.CertIssuerName, s.CertSerialNumber, s.ClassID, s.ClassName, s.CompatibleIds, s.ComputerAccountChange, s.DeviceDescription, s.DeviceID, s.AttributeLDAPDisplayName, s.AttributeSyntaxOID, s.AttributeValue, s.ObjectClass, s.ObjectDN, s.DSType, s.ObjectGUID, s.AppCorrelationID, s.OpCorrelationID, s.OperationType, s.DestAddress, s.DestPort, s.FailureReason, s.Status, s.SubStatus, s.DSName, s.PrimaryGroupID, s.LocationInformation, s.LmPackageName, s.AuthenticationPackageName, s.ElevatedToken, s.ImpersonationLevel, s.KeyLength, s.LogonProcessName, s.RestrictedAdminMode, s.TransmittedServices, s.LogonType, s.VirtualAccount, s.MasterKeyID, s.Protocol, s.AccessList, s.AccessMask, s.AccessReason, s.HandleID, s.ObjectName, s.OperationType, s.PrivilegeList, s.Properties, s.ResourceAttributes, s.ObjectServer, s.ObjectType, s.OldValueType, s.CategoryID, s.AuditPolicyChanges, s.SubcategoryGUID, s.SubcategoryID, s.ProcessID, s.ProcessName, s.ProcessName, s.RecoveryKeyID, s.ObjectName, s.NewValue, s.ObjectValueName, s.NewValueType, s.OldValue, s.TaskContent, s.TaskName, s.ServiceAccount, s.ServiceFileName, s.ServiceName, s.ServiceSid, s.ServiceStartType, s.ServiceType, s.AccessMask, s.ShareLocalPath, s.ShareName, s.RelativeTargetName, s.Workstation, s.WorkstationName, s.SrcIPAddr, s.IPPort, s.UserAccountControl, s.AccountExpires, s.AllowedToDelegateTo, s.DisplayName, s.DNSHostName, s.TargetDomainName, s.HomeDirectory, s.HomePath, s.LogonHours, s.TargetUserName, s.NewUacValue, s.OldUacValue, s.UserParameters, s.PasswordLastSet, s.UserPrincipalName, s.ProfilePath, s.SamAccountName, s.ScriptPath, s.ServicePrincipalNames, s.TargetSid, s.SidHistory, s.UserWorkstations, s.UserAccountControl, s.AccountExpires, s.AllowedToDelegateTo, s.DisplayName, s.DisplayName, s.TargetDomainName, s.HomeDirectory, s.HomePath, s.LogonHours, s.TargetUserName, s.NewTargetUserName, s.NewUacValue, s.OldTargetUserName, s.OldUacValue, s.UserParameters, s.PasswordLastSet, s.PrimaryGroupID, s.UserPrincipalName, s.ProfilePath, s.SamAccountName, s.ScriptPath, s.TargetSid, s.SidHistory, s.UserWorkstations, s.TicketEncryptionType, s.TicketEncryptionType, s.TicketOptions, s.PreAuthType, s.Status, s.RestrictedSidCount, s.TransactionID, s.TdoAttributes, s.TdoDirection, s.SidFilteringEnabled, s.DomainName, s.DomainSid, s.TdoType, s.AccessList, s.AccessReason, s.Dummy, s.SubjectDomainName, s.TargetLinkedLogonID, s.LogonGUID, s.SubjectLogonID, s.SubjectUserName, s.TargetOutboundDomainName, s.TargetOutboundUserName, s.PrimaryGroupID, s.PrivilegeList, s.SubjectDomainName, s.SubjectLogonID, s.SubjectUserName, s.SubjectUserSid, s.SubjectUserSid, s.VendorIds)
	if err != nil {
		fmt.Println("Insert Error:", err)

	}
	return err
}

// SecurityCommit write events to Clickhouse
func SecurityCommit(batch []parser.Security) {
	err, Db := Connect()
	if err != nil {
		fmt.Println("Error in Connecting to Clickhouse: ", err)
	}

	Tx, Stmt := SecurityPrepare(Db)

	for _, s := range batch {
		prom.ClickhouseEventsTotal.WithLabelValues("security").Inc()
		if err := SecurityInsert(Stmt, s); err != nil {
			fmt.Println("Insert Error: ", err)
			prom.ClickhouseEventsErrors.WithLabelValues("security").Inc()
		}
		prom.ClickhouseEventsSuccess.WithLabelValues("security").Inc()

	}
	err = Tx.Commit()

	if err != nil {
		fmt.Println("Commit Error:", err)

		Db.Close()
	}
	i := fmt.Sprintf("Security inserted! \t %d", len(batch))
	fmt.Println(i)

	Db.Close()
}
