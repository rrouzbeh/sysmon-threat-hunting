package parser

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/rrouzbeh/cyborg-hunt/pkg/prom"
	"github.com/rrouzbeh/cyborg-hunt/pkg/util"
)

func (s *Security) SecurityParser(msg []byte, eventid uint32, hostname string, ts string) {
	// endTime := util.MakeTimestamp() - startTime
	err := json.Unmarshal(msg, &s)
	if err != nil {
		fmt.Println("Error in decoding Security json:", err)

	}
	prom.KafkaEventsTotal.WithLabelValues("Security").Inc()
	s.HostName = hostname
	s.EventID = eventid
	s.EventDateCreation, _ = time.Parse("2006-01-02T15:04:05.000Z", ts)
	s.PreviousTime, _ = time.Parse("2006-01-02T15:04:05.000Z", s.PreviousTimestr)
	s.NewTime, _ = time.Parse("2006-01-02T15:04:05.000Z", s.NewTimestr)
	s.MembershipExpirationTime, _ = time.Parse("2006-01-02T15:04:05.000Z", s.MembershipExpirationTimestr)
	if s.IPAddress != "" {
		s.SrcIPAddr = s.IPAddress
	}
	if s.SourceAddress != "" {
		s.SrcIPAddr = s.SourceAddress
	}
	if s.SrcIPAddr != "" {
		s.SrcIPRfc, s.SrcIPPublic, s.SrcIPType, s.SrcIPVersion = util.IPProcess(s.SrcIPAddr)
	}

	switch eventid {
	case 4624:
		switch s.ImpersonationLevel {
		case "%%1832":
			s.ImpersonationLevel = "Identification"
		case "%%1833":
			s.ImpersonationLevel = "Impersonation"
		case "%%1840":
			s.ImpersonationLevel = "Delegation"
		case "%%1841":
			s.ImpersonationLevel = "Denied by Process Trust Label ACE"
		case "%%1842":
			s.ImpersonationLevel = "Yes"
		case "%%1843":
			s.ImpersonationLevel = "No"
		case "%%1844":
			s.ImpersonationLevel = "System"
		case "%%1845":
			s.ImpersonationLevel = "Not Available"
		case "%%1846":
			s.ImpersonationLevel = "Default"
		case "%%1847":
			s.ImpersonationLevel = "DisallowMmConfig"
		case "%%1848":
			s.ImpersonationLevel = "Off"
		case "%%1849":
			s.ImpersonationLevel = "Auto"
		case "-":
			s.ImpersonationLevel = "Anonymous"
		default:
			s.ImpersonationLevel = "Unknown"
		}
		switch s.ElevatedToken {
		case "%%1842":
			s.ElevatedToken = "Yes"
		case "%%1843":
			s.ElevatedToken = "No"
		default:
			s.ElevatedToken = "Unknown"
		}
		switch s.VirtualAccount {
		case "%%1842":
			s.VirtualAccount = "Yes"
		case "%%1843":
			s.VirtualAccount = "No"
		default:
			s.VirtualAccount = "Unknown"
		}
	case 4625:
		switch s.Status {
		case "0xc000005e":
			s.Status = "There are currently no logon servers available to service the logon request"
		case "0xc0000064":
			s.Status = "User logon with misspelled or bad user account"
		case "0xc000006a":
			s.Status = "User logon with misspelled or bad password"
		case "0xc000006d":
			s.Status = "This is either due to a bad username or authentication information"
		case "0xc000006e":
			s.Status = "Unknown user name or bad password"
		case "0xc000006f":
			s.Status = "User logon outside authorized hours"
		case "0xc0000070":
			s.Status = "User logon from unauthorized workstation"
		case "0xc0000071":
			s.Status = "User logon with expired password"
		case "0xc0000072":
			s.Status = "User logon to account disabled by administrator"
		case "0xc00000dc":
			s.Status = "Indicates the Sam Server was in the wrong state to perform the desired operation"
		case "0xc0000133":
			s.Status = "Clocks between DC and other computer too far out of sync"
		case "0xc000015b":
			s.Status = "The user has not been granted the requested logon type (aka logon right) at this machine"
		case "0xc000018c":
			s.Status = "The logon request failed because the trust relationship between the primary domain and the trusted domain failed"
		case "0xc0000192":
			s.Status = "An attempt was made to logon, but the Netlogon service was not started"
		case "0xc0000193":
			s.Status = "User logon with expired account"
		case "0xc0000224":
			s.Status = "User is required to change password at next logon"
		case "0xc0000225":
			s.Status = "Evidently a bug in Windows and not a risk"
		case "0xc0000234":
			s.Status = "User logon with account locked"
		case "0xc00002ee":
			s.Status = "Failure Reason: An Error occurred during Logon"
		case "0xc0000413":
			s.Status = "Logon Failure: The machine you are logging onto is protected by an authentication firewall. The specified account is not allowed to authenticate to the machine"
		case "0x0":
			s.Status = "Status OK"
		default:
			s.Status = "Unknown"
		}
		switch s.SubStatus {
		case "0xc000005e":
			s.SubStatus = "There are currently no logon servers available to service the logon request"
		case "0xc0000064":
			s.SubStatus = "User logon with misspelled or bad user account"
		case "0xc000006a":
			s.SubStatus = "User logon with misspelled or bad password"
		case "0xc000006d":
			s.SubStatus = "This is either due to a bad username or authentication information"
		case "0xc000006e":
			s.SubStatus = "Unknown user name or bad password"
		case "0xc000006f":
			s.SubStatus = "User logon outside authorized hours"
		case "0xc0000070":
			s.SubStatus = "User logon from unauthorized workstation"
		case "0xc0000071":
			s.SubStatus = "User logon with expired password"
		case "0xc0000072":
			s.SubStatus = "User logon to account disabled by administrator"
		case "0xc00000dc":
			s.SubStatus = "Indicates the Sam Server was in the wrong state to perform the desired operation"
		case "0xc0000133":
			s.SubStatus = "Clocks between DC and other computer too far out of sync"
		case "0xc000015b":
			s.SubStatus = "The user has not been granted the requested logon type (aka logon right) at this machine"
		case "0xc000018c":
			s.SubStatus = "The logon request failed because the trust relationship between the primary domain and the trusted domain failed"
		case "0xc0000192":
			s.SubStatus = "An attempt was made to logon, but the Netlogon service was not started"
		case "0xc0000193":
			s.SubStatus = "User logon with expired account"
		case "0xc0000224":
			s.SubStatus = "User is required to change password at next logon"
		case "0xc0000225":
			s.SubStatus = "Evidently a bug in Windows and not a risk"
		case "0xc0000234":
			s.SubStatus = "User logon with account locked"
		case "0xc00002ee":
			s.SubStatus = "Failure Reason: An Error occurred during Logon"
		case "0xc0000413":
			s.SubStatus = "Logon Failure: The machine you are logging onto is protected by an authentication firewall. The specified account is not allowed to authenticate to the machine"
		case "0x0":
			s.SubStatus = "Status OK"
		default:
			s.SubStatus = "Unknown"
		}
	case 4776:
		switch s.Status {
		case "0xc0000064":
			s.Status = "The username you typed does not exist. Bad username"
		case "0xc000006a":
			s.Status = "Account logon with misspelled or bad password"
		case "0xc000006d":
			s.Status = "Generic logon failure Some of the potential causes for this: An invalid username and/or password was used LAN Manager Authentication Level mismatch between the source and target computers"
		case "0xc000006f":
			s.Status = "Account logon outside authorized hours"
		case "0xc0000070":
			s.Status = "Account logon from unauthorized workstation"
		case "0xc0000071":
			s.Status = "Account logon with expired password"
		case "0xc0000072":
			s.Status = "Account logon to account disabled by administrator"
		case "0xc0000193":
			s.Status = "Account logon with expired account"
		case "0xc0000224":
			s.Status = "Account logon with Change Password at Next Logon flagged"
		case "0xc0000234":
			s.Status = "Account logon with account locked"
		case "0xc0000371":
			s.Status = "The local account store does not contain secret material for the specified account"
		case "0x0":
			s.Status = "No errors"
		default:
			s.Status = "Unknown"
		}
	case 4662:
		switch s.ObjectType {
		case "f30e3bc2-9ff0-11d1-b603-0000f80367c1":
			s.ObjectType = "Group-Policy-Container"
		case "19195a5b-6da0-11d0-afd3-00c04fd930c9":
			s.ObjectType = "Domain-DNS"
		case "bf967aa5-0de6-11d0-a285-00aa003049e2":
			s.ObjectType = "Organizational-Unit"
		case "bf967a8b-0de6-11d0-a285-00aa003049e2":
			s.ObjectType = "Container"
		case "bf967a86-0de6-11d0-a285-00aa003049e2":
			s.ObjectType = "Computer"
		case "2628a46a-a6ad-4ae0-b854-2b12d9fe6f9e":
			s.ObjectType = "Account"
		case "bf967a9c-0de6-11d0-a285-00aa003049e2":
			s.ObjectType = "Group"
		}

	}
	// batch = append(batch, s)
	// if len(batch) >= commitAfter || endTime >= int64(idleTime) {

	// 	click.SysmonCommit(batch)
	// 	fmt.Println(len(batch))
	// 	batch = batch[:0]
	// 	startTime = util.MakeTimestamp()
}
