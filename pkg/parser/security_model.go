package parser

import (
	"time"
)

type Security struct {
	HostName                    string
	ComputerName                string
	EventID                     uint32
	EventDateCreation           time.Time
	SubjectUserSid              string `json:"SubjectUserSid"`
	SubjectUserName             string `json:"SubjectUserName"`
	SubjectDomainName           string `json:"SubjectDomainName"`
	SubjectLogonID              string `json:"SubjectLogonId"`
	ObjectServer                string `json:"ObjectServer"`
	ObjectType                  string `json:"ObjectType"`
	ObjectName                  string `json:"ObjectName"`
	HandleID                    string `json:"HandleId"`
	AccessMask                  string `json:"AccessMask"`
	PrivilegeList               string `json:"PrivilegeList"`
	ProcessID                   string `json:"ProcessId"`
	ProcessName                 string `json:"ProcessName"`
	Application                 string `json:"Application"`
	Direction                   string `json:"Direction"`
	SourceAddress               string `json:"SourceAddress"`
	SourcePort                  string `json:"SourcePort"`
	DestAddress                 string `json:"DestAddress"`
	DestPort                    string `json:"DestPort"`
	Protocol                    string `json:"Protocol"`
	LayerName                   string `json:"LayerName"`
	RemoteUserID                string `json:"RemoteUserID"`
	RemoteMachineID             string `json:"RemoteMachineID"`
	ObjectValueName             string `json:"ObjectValueName"`
	OperationType               string `json:"OperationType"`
	OldValueType                string `json:"OldValueType"`
	OldValue                    string `json:"OldValue"`
	NewValueType                string `json:"NewValueType"`
	NewValue                    string `json:"NewValue"`
	TransactionID               string `json:"TransactionId"`
	AccessList                  string `json:"AccessList"`
	AccessReason                string `json:"AccessReason"`
	RestrictedSidCount          string `json:"RestrictedSidCount"`
	ResourceAttributes          string `json:"ResourceAttributes"`
	SourceUserName              string `json:"SourceUserName"`
	SourceSid                   string `json:"SourceSid"`
	TargetUserName              string `json:"TargetUserName"`
	TargetDomainName            string `json:"TargetDomainName"`
	TargetSid                   string `json:"TargetSid"`
	SidList                     string `json:"SidList"`
	ServiceName                 string `json:"ServiceName"`
	ServiceSid                  string `json:"ServiceSid"`
	TicketOptions               string `json:"TicketOptions"`
	TicketEncryptionType        string `json:"TicketEncryptionType"`
	SrcIPAddr                   string
	SrcIPRfc                    string
	SrcIPPublic                 string
	SrcIPType                   string
	SrcIPVersion                string
	IPAddress                   string `json:"IpAddress"`
	IPPort                      string `json:"IpPort"`
	Status                      string `json:"Status"`
	LogonGUID                   string `json:"LogonGuid"`
	TransmittedServices         string `json:"TransmittedServices"`
	PreAuthType                 string `json:"PreAuthType"`
	CertIssuerName              string `json:"CertIssuerName"`
	CertSerialNumber            string `json:"CertSerialNumber"`
	CertThumbprint              string `json:"CertThumbprint"`
	OldTargetUserName           string `json:"OldTargetUserName"`
	NewTargetUserName           string `json:"NewTargetUserName"`
	PreviousTime                time.Time
	PreviousTimestr             string `json:"PreviousTime"`
	NewTime                     time.Time
	NewTimestr                  string `json:"NewTime"`
	DomainName                  string `json:"DomainName"`
	DomainSid                   string `json:"DomainSid"`
	TdoType                     uint32 `json:"TdoType"`
	TdoDirection                uint32 `json:"TdoDirection"`
	TdoAttributes               uint32 `json:"TdoAttributes"`
	SidFilteringEnabled         string `json:"SidFilteringEnabled"`
	MasterKeyID                 string `json:"MasterKeyId"`
	RecoveryServer              string `json:"RecoveryServer"`
	RecoveryKeyID               string `json:"RecoveryKeyId"`
	FailureReason               string `json:"FailureReason"`
	ServiceFileName             string `json:"ServiceFileName"`
	ServiceType                 string `json:"ServiceType"`
	ServiceStartType            string `json:"ServiceStartType"`
	ServiceAccount              string `json:"ServiceAccount"`
	ComputerAccountChange       string `json:"ComputerAccountChange"`
	SamAccountName              string `json:"SamAccountName"`
	DisplayName                 string `json:"DisplayName"`
	UserPrincipalName           string `json:"UserPrincipalName"`
	HomeDirectory               string `json:"HomeDirectory"`
	HomePath                    string `json:"HomePath"`
	ScriptPath                  string `json:"ScriptPath"`
	ProfilePath                 string `json:"ProfilePath"`
	UserWorkstations            string `json:"UserWorkstations"`
	PasswordLastSet             string `json:"PasswordLastSet"`
	AccountExpires              string `json:"AccountExpires"`
	PrimaryGroupID              string `json:"PrimaryGroupId"`
	AllowedToDelegateTo         string `json:"AllowedToDelegateTo"`
	OldUacValue                 string `json:"OldUacValue"`
	NewUacValue                 string `json:"NewUacValue"`
	UserAccountControl          string `json:"UserAccountControl"`
	UserParameters              string `json:"UserParameters"`
	SidHistory                  string `json:"SidHistory"`
	LogonHours                  string `json:"LogonHours"`
	DNSHostName                 string `json:"DnsHostName"`
	ServicePrincipalNames       string `json:"ServicePrincipalNames"`
	DeviceID                    string `json:"DeviceId"`
	DeviceDescription           string `json:"DeviceDescription"`
	ClassID                     string `json:"ClassId"`
	ClassName                   string `json:"ClassName"`
	VendorIds                   string `json:"VendorIds"`
	CompatibleIds               string `json:"CompatibleIds"`
	LocationInformation         string `json:"LocationInformation"`
	Service                     string `json:"Service"`
	ShareName                   string `json:"ShareName"`
	ShareLocalPath              string `json:"ShareLocalPath"`
	RelativeTargetName          string `json:"RelativeTargetName"`
	PackageName                 string `json:"PackageName"`
	Workstation                 string `json:"Workstation"`
	TargetUserSid               string `json:"TargetUserSid"`
	TargetLogonID               string `json:"TargetLogonId"`
	LogonType                   string `json:"LogonType"`
	LogonProcessName            string `json:"LogonProcessName"`
	AuthenticationPackageName   string `json:"AuthenticationPackageName"`
	WorkstationName             string `json:"WorkstationName"`
	LmPackageName               string `json:"LmPackageName"`
	KeyLength                   string `json:"KeyLength"`
	ImpersonationLevel          string `json:"ImpersonationLevel"`
	RestrictedAdminMode         string `json:"RestrictedAdminMode"`
	TargetOutboundUserName      string `json:"TargetOutboundUserName"`
	TargetOutboundDomainName    string `json:"TargetOutboundDomainName"`
	VirtualAccount              string `json:"VirtualAccount"`
	TargetLinkedLogonID         string `json:"TargetLinkedLogonId"`
	ElevatedToken               string `json:"ElevatedToken"`
	SubStatus                   string `json:"SubStatus"`
	CategoryID                  string `json:"CategoryId"`
	SubcategoryID               string `json:"SubcategoryId"`
	SubcategoryGUID             string `json:"SubcategoryGuid"`
	AuditPolicyChanges          string `json:"AuditPolicyChanges"`
	MemberName                  string `json:"MemberName"`
	MemberSid                   string `json:"MemberSid"`
	MembershipExpirationTimestr string `json:"MembershipExpirationTime"`
	MembershipExpirationTime    time.Time
	Dummy                       string `json:"Dummy"`
	Properties                  string `json:"Properties"`
	AdditionalInfo              string `json:"AdditionalInfo"`
	AdditionalInfo2             string `json:"AdditionalInfo2"`
	TaskName                    string `json:"TaskName"`
	TaskContent                 string `json:"TaskContent"`
	OpCorrelationID             string `json:"OpCorrelationID"`
	AppCorrelationID            string `json:"AppCorrelationID"`
	DSName                      string `json:"DSName"`
	DSType                      string `json:"DSType"`
	ObjectDN                    string `json:"ObjectDN"`
	ObjectGUID                  string `json:"ObjectGUID"`
	ObjectClass                 string `json:"ObjectClass"`
	AttributeLDAPDisplayName    string `json:"AttributeLDAPDisplayName"`
	AttributeSyntaxOID          string `json:"AttributeSyntaxOID"`
	AttributeValue              string `json:"AttributeValue"`
}
