package service

import (
	"strconv"
	"time"

	"github.com/prometheus/common/log"
	"github.com/rrouzbeh/cyborg-hunt/pkg/click"
	"github.com/rrouzbeh/cyborg-hunt/pkg/kafka"
	"github.com/rrouzbeh/cyborg-hunt/pkg/parser"
	"github.com/rrouzbeh/cyborg-hunt/pkg/util"
)

type Service struct {
	stopped chan struct{}

	Name          string
	FlushInterval int
	BufferSize    int
	MinBufferSize int
}

type Msg struct{}

func Init() *Service {
	s := &Service{}
	s.Name = util.GetEnv("KAFKA_CONSUMER_NAME", "symon-consumer")
	s.FlushInterval, _ = strconv.Atoi(util.GetEnv("FLUSH_INTERVAL", "20"))
	s.BufferSize, _ = strconv.Atoi(util.GetEnv("BUFFER_SIZE", "1000"))
	s.MinBufferSize, _ = strconv.Atoi(util.GetEnv("MIN_BUFFER_SIZE", "10"))

	return s
}

func KafkaConfig() *kafka.Kafka {

	k := kafka.NewKafka()
	k.Name = util.GetEnv("KAFKA_CONSUMER_NAME", "symon-consumer")
	k.Version = util.GetEnv("KAFKA_VERSION", "2.1.1")
	k.Oldest = util.ParseBool(util.GetEnv("KAFKA_OLDEST", "true"))
	k.Brokers = util.GetEnv("KAFKA_BROKERS", "82.99.244.114:9092")
	k.Group = util.GetEnv("KAFKA_GROUP", "cyborgsssss-sysmsssson2ssxx12x2")
	k.Topic = util.GetEnv("KAFKA_TOPICS", "winlogbeat")

	return k
}

func (service *Service) Run() {
	k := KafkaConfig()
	k.Init()

	if err := k.Start(); err != nil {
		panic(err)
	}

	sysmonTick := time.NewTicker(time.Duration(service.FlushInterval) * time.Second)
	securityTick := time.NewTicker(time.Duration(service.FlushInterval) * time.Second)
	sysmon_msgs := make([]parser.Sysmon, 0, service.BufferSize)
	security_msgs := make([]parser.Security, 0, service.BufferSize)
FOR:
	for {
		select {
		case msg, more := <-k.Msgs():
			if !more {
				break FOR
			}
			if len(msg) > 0 {
				sec, s, t := parser.EventHandler(msg)
				if t == "sysmon" {
					sysmon_msgs = append(sysmon_msgs, s)
					if len(sysmon_msgs) >= service.BufferSize {
						click.SysmonCommit(sysmon_msgs)
						sysmon_msgs = sysmon_msgs[:0]
						sysmonTick = time.NewTicker(time.Duration(service.FlushInterval) * time.Second)
					}
				}
				if t == "security" {
					security_msgs = append(security_msgs, sec)
					if len(security_msgs) >= service.BufferSize {
						click.SecurityCommit(security_msgs)
						security_msgs = security_msgs[:0]
						securityTick = time.NewTicker(time.Duration(service.FlushInterval) * time.Second)
					}
				}
			}

		case <-sysmonTick.C:
			log.Infof("%s: tick", "Sysmon Logs")
			if len(sysmon_msgs) == 0 || len(sysmon_msgs) < service.MinBufferSize {
				continue
			}
			click.SysmonCommit(sysmon_msgs)
			sysmon_msgs = sysmon_msgs[:0]
		case <-securityTick.C:
			log.Infof("%s: tick", "Security Logs")
			if len(security_msgs) == 0 || len(security_msgs) < service.MinBufferSize {
				continue
			}
			click.SecurityCommit(security_msgs)
			security_msgs = security_msgs[:0]
		}

	}

	click.SysmonCommit(sysmon_msgs)
	click.SecurityCommit(security_msgs)
	service.stopped <- struct{}{}
}
