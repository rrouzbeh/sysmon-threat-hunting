package main

import (
	"log"
	"os"
	"sync"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/rrouzbeh/cyborg-hunt/pkg/prom"
	"github.com/rrouzbeh/cyborg-hunt/pkg/service"
	"github.com/rrouzbeh/cyborg-hunt/pkg/util"
)

var (
	wg        sync.WaitGroup
	logger    = log.New(os.Stderr, "", log.LstdFlags)
	eventType = util.GetEnv("EVENT_TYPE", "sysmon")
)

func main() {
	prometheus.MustRegister(prom.ClickhouseEventsSuccess)
	prometheus.MustRegister(prom.ClickhouseEventsErrors)
	prometheus.MustRegister(prom.ClickhouseEventsTotal)
	prometheus.MustRegister(prom.KafkaEventsTotal)
	wg.Add(1)
	go prom.Metrics()
	s := service.Init()
	s.Run()

	wg.Wait()

	// msgs := k.Msgs()
	// wg.Add(1)
	// switch eventType {
	// case "sysmon":
	// 	go parser.Sysmon(msgs)
	// }
	// go prom.Metrics()
	// wg.Wait()

}
